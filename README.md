# Familliars Recognition
Описание ПО
--------------
ПО предоставляет возможность распознать на фотографии людей, присутствующих в базе данных (вместе с фотографиями), получить информацию о каждом из людей в базе данных, а также добавлять/удалять факты о них и добавлять записи диалогов с ними.  
Изначально, данная программа моделировала компонент робота, отвечающий за распознание знакомых ему людей и изменение информации о них в ходе разговора.  

Функциональные требования
--------------
* Возможность по фотографии получить список ID записей в БД о людях, распознанных на ней.
* Возможность по заданному ID получиьт информацию очеловеке из БД.
* Возможность добавить запись разговора (фраза человека + фраза бота) в базу данных.
* Возможность добавить факт о человеке в базу данных
* Возможность удалить факт о человеке из базы данных

ER диаграмма сущностей
----------------------
![](https://gitlab.com/Rabter/web-development/-/raw/lab_1/lab_1/ER.png)

Use-Case диаграмма
----------------------
![](https://gitlab.com/Rabter/web-development/-/raw/lab_1/lab_1/Use-Case.png)

Apache Benchmark
----------------
**Результат при 1 рабочем порте**
```
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        nginx/1.18.0
Server Hostname:        localhost
Server Port:            80

Document Path:          /api/v1
Document Length:        Variable

Concurrency Level:      10
Time taken for tests:   4.987 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      3810000 bytes
HTML transferred:       1780000 bytes
Requests per second:    2005.15 [#/sec] (mean)
Time per request:       4.987 [ms] (mean)
Time per request:       0.499 [ms] (mean, across all concurrent requests)
Transfer rate:          746.06 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    2   2.8      1     105
Processing:     0    3   3.4      2     117
Waiting:        0    2   2.5      2      95
Total:          1    5   5.1      4     145

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      4
  75%      5
  80%      6
  90%      8
  95%     11
  98%     16
  99%     21
 100%    145 (longest request)
```

**Результат при 3 рабочих портах с балансировкой 1:1:1**
```
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        nginx/1.18.0
Server Hostname:        localhost
Server Port:            80

Document Path:          /api/v1
Document Length:        Variable

Concurrency Level:      10
Time taken for tests:   3.134 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      3810000 bytes
HTML transferred:       1780000 bytes
Requests per second:    3190.81 [#/sec] (mean)
Time per request:       3.134 [ms] (mean)
Time per request:       0.313 [ms] (mean, across all concurrent requests)
Transfer rate:          1187.21 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.6      1      14
Processing:     1    2   0.9      2      16
Waiting:        0    1   0.7      1      14
Total:          2    3   1.2      3      17

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      3
  75%      3
  80%      3
  90%      4
  95%      5
  98%      6
  99%      8
 100%     17 (longest request)
```

**Результат при 3 рабочих портах с балансировкой 2:1:1**
```
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        nginx/1.18.0
Server Hostname:        localhost
Server Port:            80

Document Path:          /api/v1
Document Length:        Variable

Concurrency Level:      10
Time taken for tests:   7.618 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      3810000 bytes
HTML transferred:       1780000 bytes
Requests per second:    1312.64 [#/sec] (mean)
Time per request:       7.618 [ms] (mean)
Time per request:       0.762 [ms] (mean, across all concurrent requests)
Transfer rate:          488.39 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    3   5.9      2     163
Processing:     1    4   7.5      2     165
Waiting:        0    3   5.9      2     163
Total:          1    7  11.4      4     227

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      6
  75%      7
  80%      8
  90%     12
  95%     18
  98%     33
  99%     51
 100%    227 (longest request)
```

Макет Figma
-----------
[Familliars Recognition](https://www.figma.com/file/Hoh0iUC3SRIafnQhnKNHZt/Untitled?node-id=2%3A2)
