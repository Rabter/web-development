﻿using System.IO;

public static class Config
{
    private static bool initialized = false;
    private static string configurationFile = @"D:\University\web\lab_1\Config\config.ini";
    private static string dataBaseType;
    private static string connectionString;
    private static string imagesPath;

    public static void init()
    {
        using (StreamReader fconf = File.OpenText(configurationFile))
        {
            string line;
            while ((line = fconf.ReadLine()) != null)
            {
                string[] param = line.Split(":".ToCharArray(), 2);
                switch (param[0])
                {
                    case "DBType":
                        dataBaseType = param[1];
                        break;
                    case "ConnectionString":
                        connectionString = param[1];
                        break;
                    case "ImagesPath":
                        imagesPath = param[1];
                        break;
                }
            }
        }
        initialized = true;
    }

    public static string DataBaseType()
    {
        if (!initialized)
            init();
        return dataBaseType;
    }

    public static string ConnectionString()
    {
        if (!initialized)
            init();
        return connectionString;
    }
    public static string ImagesPath()
    {
        if (!initialized)
            init();
        return imagesPath;
    }
}