import sys
import face_recognition

if __name__ == "__main__":
    path = sys.argv[1]
    f = open(path + '\\result.txt', 'w')
    encodings1 = face_recognition.face_encodings(face_recognition.load_image_file(path + '\\' + sys.argv[2]))
    encodings2 = face_recognition.face_encodings(face_recognition.load_image_file(path + '\\' + sys.argv[3]))
    if len(encodings1) and len(encodings2):
        go = True
        i = 0
        while go and i < len(encodings2):
            enc2 = encodings2[i]
            results = face_recognition.compare_faces(encodings1, enc2, 0.6)
            j = 0
            while j < len(results) and go:
                if results[j]:
                    go = False
                j += 1
            i += 1
        if go:
            f.write('0')
        else:
            f.write('1')
    else:
        f.write('0')

