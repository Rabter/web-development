﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace FamilliarsRecognition
{
    public class FamilliarsManager: IDisposable
    {
        public FamilliarsManager()
        {
            db_manager = new DBAccess.PeopleStorage();
            loader = new FamilliarsLoader();
        }

        public void Dispose() => db_manager.Dispose();

        public List<int> HandleNewImage(in Image image)
        {
            EventLog.Log("Started handling new image");
            List<int> people = loader.SelectPeople(image, db_manager);
            db_manager.LoadPeople(people);
            EventLog.Log("Finished handling new image");
            return people;
        }

        public bool GetPerson(int id, out Person person)
        {
            DBAccess.Person factless_person;
            if (!db_manager.GetPerson(id, out factless_person))
            {
                person = default;
                return false;
            }
            person = new Person(factless_person);
            person.facts = new List<Fact>();
            foreach (DBAccess.Fact fact in db_manager.GetFacts(id))
                person.facts.Add(new Fact(fact));
            person.logs = new List<LogLine>();
            foreach (DBAccess.LogLine log in db_manager.GetLogs(id))
                person.logs.Add(new LogLine(log));
            Image photo;
            db_manager.GetImage(id, out photo);
            person.photo = photo;
            return true;
        }

        public void AddPairs(List<int> ids)
        {
            int sizedec = ids.Count - 1;
            for (int i = 0; i < sizedec; ++i)
                for (int j = i + 1; j < ids.Count; ++j)
                    db_manager.AddPair(ids[i], ids[j]);
        }

        public void AddFact(in Fact fact)
        {
            db_manager.AddFact(fact);
            EventLog.Log("Added fact to person " + fact.person_id);
        }

        public void DeleteFact(in Fact fact)
        {
            db_manager.DeleteFact(fact);
            EventLog.Log("Deleted fact from person " + fact.person_id);
        }

        public void AddLog(in LogLine log)
        {
            db_manager.AddLog(log);
            EventLog.Log("Added log to person " + log.person_id);
        }

        static DBAccess.PeopleStorage db_manager;
        FamilliarsLoader loader;
    }

    class FamilliarsLoader
    {
        public List<int> SelectPeople(in Image image, in DBAccess.PeopleStorage manager)
        {
            EventLog.Log("Started selecting people");
            int id = manager.MinId(), max = manager.MaxId();
            List<int> res = new List<int>(0);
            while (id <= max)
            {
                if (manager.GetImage(id, out Image photo) && Compare(image, photo))
                {
                    EventLog.Log("Recognized id " + id);
                    res.Add(id);
                }
                id++;
            }
            EventLog.Log("Finished selecting people");
            return res;
        }

        bool Compare(in Image img1, in Image img2)
        {
            img1.Save(comparator_dir + @"\1.jpg"); // ToDo: boost using memory-mapped files
            img2.Save(comparator_dir + @"\2.jpg");
            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
            {
                process.StartInfo.FileName = python_path;
                process.StartInfo.Arguments = comparator_dir + @"\" + comparator_name + " " + comparator_dir + " 1.jpg 2.jpg";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.WaitForExit();
            }
            return (System.IO.File.ReadAllLines(comparator_dir + @"\result.txt")[0] == "1");
        }

        static string python_path = @"D:\Programs\Python\python.exe";
        static string comparator_dir = @"D:\University\DB_Project\compare";
        static string comparator_name = "compare.py";
    }

    public class Person: DBAccess.Person
    {
        public Person(DBAccess.Person parent)
        {
            id = parent.id;
            name = parent.name;
            sex = parent.sex;
            birthdate = parent.birthdate;
            facts = null;
            logs = default;
            photo = default;
        }

        ~Person()
        {
            if (photo != null)
                photo.Dispose();
        }

        public override string ToString()
        {
            string split = "; ";
            string res = id.ToString() + split + name + split + sex + split + birthdate.ToShortDateString();
            foreach (Fact fact in facts)
                res += split + fact.fact;
            return res;
        }

        public List<Fact> facts { get; set; }
        public List<LogLine> logs { get; set; }
        public Image photo { get; set; }
    }

    public class LogLine: DBAccess.LogLine
    {
        public LogLine(in DBAccess.LogLine log): base(log) {}
        public LogLine(int person_id, in DateTime datetime, in string person_phrase, in string bot_phrase):
            base(person_id, datetime, person_phrase, bot_phrase) {}
    }
    public class Fact: DBAccess.Fact
    {
        public Fact(): base(-1, "") {}
        public Fact(in DBAccess.Fact fact): base(fact) {}
        public Fact(int person_id, in string fact): base(person_id, fact) {}
    }

}
