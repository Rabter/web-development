﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FamilliarsRecognition
{
    public class JWTAuthorizer
    {
        public static bool Authorize(string token)
        {
            bool valid;
            if (token == null)
                return false;
            string[] items = token.Split('.');
            if (items.Length != 3)
                return false;
            byte[] key = System.Text.Encoding.UTF8.GetBytes("Rabter");
            string data = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(items[0])) + '.' + System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(items[1]));
            using (HMACSHA256 hmac = new HMACSHA256(key))
            {
                byte[] rawdata = System.Text.Encoding.UTF8.GetBytes(data);
                byte[] hashValue = hmac.ComputeHash(rawdata);
                valid = System.Convert.ToBase64String(hashValue) == items[2];
            }
            return valid;
        }

        public static string Generate(string header, string payload)
        {
            byte[] key = System.Text.Encoding.UTF8.GetBytes("Rabter");
            byte[] data = System.Text.Encoding.UTF8.GetBytes(header + '.' + payload);
            string token = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(header)) + '.' + System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(payload));
            using (HMACSHA256 hmac = new HMACSHA256(key))
            {
                byte[] hashValue = hmac.ComputeHash(data);
                token += '.' + System.Convert.ToBase64String(hashValue);
            }
            return token;
        }
    }

    public class JWTAuthrizationHandler : IAuthenticationHandler
    {
        private HttpContext _context;

        public Task InitializeAsync(AuthenticationScheme scheme, HttpContext context)
        {
            _context = context;
            return Task.CompletedTask;
        }

        public Task<AuthenticateResult> AuthenticateAsync()
            => Task.FromResult(AuthenticateResult.NoResult());

        public Task ChallengeAsync(AuthenticationProperties properties)
        {
            return Task.FromResult(AuthenticateResult.Fail("Authentication Failed for your token"));
        }

        public Task ForbidAsync(AuthenticationProperties properties)
        {
            return Task.FromResult(AuthenticateResult.Fail("Authentication Failed for your token"));
        }
    }
}
