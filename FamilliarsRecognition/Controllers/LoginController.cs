﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace FamilliarsRecognition.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private FamilliarsManager manager;

        public LoginController()
        {
            manager = new FamilliarsManager();
        }

        /// <summary>
        /// Selects all familliars on the imagea given in the body and returns a list of their IDs.
        /// </summary>
        // GET: api/<FamilliarsController>
        [HttpGet]
        public string Get([FromQuery] string login, [FromQuery] string password)
        {
            return JWTAuthorizer.Generate("{\n\"alg\": \"HS256\",\n  \"typ\": \"JWT\"\n}", "{\n\"user\": \"" + login + "\",\n  \"password\": \"" + password + "\"\n}");
        }
    }
}
