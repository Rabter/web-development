﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;

namespace FamilliarsRecognition.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FamilliarsController: ControllerBase
    {
        private FamilliarsManager manager;

        public FamilliarsController()
        {
            manager = new FamilliarsManager();
        }

        /// <summary>
        /// Selects all familliars on the imagea given in the body and returns a list of their IDs.
        /// </summary>
        // GET: api/<FamilliarsController>
        [HttpGet]
        public List<int> Get()
        {
            Image image = Image.FromStream(HttpContext.Request.Body);
            return manager.HandleNewImage(image);
        }

        /// <summary>
        /// Returns a person by ID.
        /// </summary>
        // GET api/<FamilliarsController>/5
        [HttpGet("{id}")]
        public Person Get(int id)
        {
            Person person;
            manager.GetPerson(id, out person);
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return person;
        }

        [HttpPatch("{id}")]
        public ActionResult Patch(int id, List<Fact> facts, [FromQuery] string token)
        {
            if (JWTAuthorizer.Authorize(token))
            {
                Person person;
                if (!manager.GetPerson(id, out person))
                    return BadRequest();
                foreach (Fact fact in person.facts.Except(facts))
                    manager.DeleteFact(fact);
                foreach (Fact fact in facts.Except(person.facts))
                    manager.AddFact(fact);
                HttpContext.Response.StatusCode = 200;
                return Ok();
            }
            HttpContext.Response.StatusCode = 403;
            return Forbid();
        }
    }
}
