﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamilliarsRecognition.Controllers
{
    [Route("api/v1/Familliars/{id?}/[controller]")]
    [ApiController]
    public class LogsController : Controller
    {
        private FamilliarsManager manager;

        public LogsController()
        {
            manager = new FamilliarsManager();
        }

        public class Phrases
        {
            public string person_phrase { get; set; }
            public string bot_phrase { get; set; }
        }
        /// <summary>
        /// Adds a new dialog log line to the person.
        /// </summary>
        // PUT api/<FamilliarsController>
        [HttpPost]
        public void Post(int? id, [FromQuery] DateTime time, [FromBody] Phrases phrases, [FromQuery] string token)
        {
            if (JWTAuthorizer.Authorize(token))
            {
                manager.AddLog(new LogLine((int)id, time, phrases.person_phrase, phrases.bot_phrase));
                HttpContext.Response.StatusCode = 200;
            }
            else
                HttpContext.Response.StatusCode = 403;
        }
    }
}

