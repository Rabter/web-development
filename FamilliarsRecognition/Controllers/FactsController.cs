﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamilliarsRecognition.Controllers
{
    [Route("api/v1/Familliars/{id?}/[controller]")]
    [ApiController]
    public class FactsController : Controller
    {
        private FamilliarsManager manager;

        public FactsController()
        {
            manager = new FamilliarsManager();
        }


        /// <summary>
        /// Adds a new fact to the person.
        /// </summary>
        [HttpPost]
        public void Post(int? id, [FromQuery] string fact, [FromQuery] string token)
        {
            if (JWTAuthorizer.Authorize(token))
            {
                manager.AddFact(new Fact((int)id, fact));
                HttpContext.Response.StatusCode = 200;
            }
            else
                HttpContext.Response.StatusCode = 403;
        }

        /// <summary>
        /// Deletes specified fact.
        /// </summary>
        [HttpDelete]
        public void Delete(int? id, [FromQuery] string fact, [FromQuery] string token)
        {
            if (JWTAuthorizer.Authorize(token))
            {
                manager.DeleteFact(new Fact((int)id, fact));
                HttpContext.Response.StatusCode = 200;
            }
            else
                HttpContext.Response.StatusCode = 403;
        }
    }
}
