﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;

namespace DBAccess
{
    internal class MSSQLAccessor: Accessor
    {
        internal MSSQLAccessor()
        {
            connection_line = Config.ConnectionString(); //"data source = (localdb)\\MSSQLLocalDB; Initial Catalog = Familliars";
        }

        public bool GetPerson(ref Person person)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select name, sex, birthdate from People where id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = person.id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        person.name = reader.GetString(0);
                        person.sex = reader.GetString(1);
                        person.birthdate = reader.GetDateTime(2);
                        return true;
                    }
                    EventLog.Log("DB: Requested person does not exist", EventLog.Type.Error);
                    return false;
                }
            }
        }

        public bool GetName(int id, out string name)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select name from People where id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        name = reader.GetString(0);
                        return true;
                    }
                    name = "";
                    EventLog.Log("DB: Requested name does not exist", EventLog.Type.Error);
                    return false;
                }
            }
        }

        public bool GetImage(int id, out Image image)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select picture from People where id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        System.IO.Stream imgStream = reader.GetStream(0);
                        if (imgStream.Length == 0)
                        {
                            image = default;
                            return false;
                        }
                        image = Image.FromStream(imgStream);
                        return true;
                    }
                    image = default;
                    EventLog.Log("DB: Requested image does not exist", EventLog.Type.Error);
                    return false;
                }
            }
        }

        public bool GetSex(int id, out string sex)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select sex from People where id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        sex = reader.GetString(0);
                        return true;
                    }
                    sex = "";
                    EventLog.Log("DB: Requested sex does not exist", EventLog.Type.Error);
                    return false;
                }
            }
        }

        public bool GetBirthdate(int id, out DateTime birthdate)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select birthdate from People where id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        birthdate = reader.GetDateTime(0);
                        return true;
                    }
                    birthdate = new DateTime();
                    EventLog.Log("DB: Requested birthdate does not exist", EventLog.Type.Error);
                    return false;
                }
            }
        }

        public List<Fact> GetFacts(int id)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select fact from Facts where person_id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    List<Fact> facts = new List<Fact>();
                    while (reader.Read())
                        facts.Add(new Fact(id, reader.GetString(0)));
                    return facts;
                }
            }
        }

        public List<LogLine> GetLogs(int person_id)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select log_time, person_phrase, bot_phrase from Logs where person_id = @id;";
                    SqlParameter parameter = new SqlParameter("@id", SqlDbType.TinyInt) { Value = person_id };
                    command.Parameters.Add(parameter);
                    SqlDataReader reader = command.ExecuteReader();
                    List<LogLine> logs = new List<LogLine>();
                    while (reader.Read())
                    {
                        LogLine log = new LogLine(person_id, reader.GetDateTime(0), reader.GetString(1), reader.GetString(2));
                        logs.Add(log);
                    }
                    return logs;
                }
            }
        }

        public bool AddPair(int id1, int id2)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into CommonFamilliars(id1, id2) values(" + id1 + "," + id2 + ")";
                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        public bool AddFact(Fact fact)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into Facts(person_id, fact) values(" + fact.person_id + ",'" + fact.fact + "')";
                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        public bool AddLog(LogLine log)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "insert into Logs (person_id, log_time, person_phrase, bot_phrase) values (@id, @datetime, @person, @bot)";
                    command.Parameters.Add(new SqlParameter("@id", SqlDbType.TinyInt) { Value = log.person_id });
                    command.Parameters.Add(new SqlParameter("@datetime", SqlDbType.DateTime) { Value = log.datetime });
                    command.Parameters.Add(new SqlParameter("@person", SqlDbType.VarChar) { Value = log.person_phrase });
                    command.Parameters.Add(new SqlParameter("@bot", SqlDbType.VarChar) { Value = log.bot_phrase });
                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        public void DeleteFact(Fact fact)
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "delete from Facts where person_id = " + fact.person_id + " and fact = '" + fact.fact + "'";
                    command.ExecuteNonQuery();
                }
            }
        }

        public int MinId()
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select min(id) from People";
                    return (int)command.ExecuteScalar();
                }
            }
        }

        public int MaxId()
        {
            using (SqlConnection connection = new SqlConnection(connection_line))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "select max(id) from People";
                    return (int)command.ExecuteScalar();
                }
            }
        }

        private string connection_line;
    }
}
