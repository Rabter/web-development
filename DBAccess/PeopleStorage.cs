﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace DBAccess
{
    public class PeopleStorage: IDisposable
    {
        public PeopleStorage()
        {
            switch (Config.DataBaseType())
            {
                case "MSSQL":
                    accessor = new MSSQLAccessor();
                    break;
                case "MySQL":
                    accessor = new MySQLAccessor();
                    break;
                default:
                    throw new InvalidOperationException();
            }
            people = new Dictionary<int, Person>();
            facts = new Dictionary<int, List<Fact>>();
            added_facts = new Dictionary<int, List<Fact>>();
            deleted_facts = new Dictionary<int, List<Fact>>();
            logs = new Dictionary<int, List<LogLine>>();
            added_logs = new Dictionary<int, List<LogLine>>();
            loaded_ids = new List<int>();
        }

        public void Dispose()
        {
            foreach (int id in loaded_ids)
            {
                if (added_facts.ContainsKey(id))
                {
                    foreach (Fact fact in added_facts[id])
                        accessor.AddFact(fact);
                    added_facts.Remove(id);
                }
                if (deleted_facts.ContainsKey(id))
                {
                    foreach (Fact fact in deleted_facts[id])
                        accessor.DeleteFact(fact);
                    deleted_facts.Remove(id);
                }
                if (added_logs.ContainsKey(id))
                {
                    foreach (LogLine log in added_logs[id])
                        accessor.AddLog(log);
                    added_logs.Remove(id);
                }

                people.Remove(id);
                facts.Remove(id);
                logs.Remove(id);
            }
        }

        public int MinId() => accessor.MinId();
        public int MaxId() => accessor.MaxId();

        public void LoadPeople(List<int> ids)
        {
            EventLog.Log("Started loading recognized people");
            // Remove records that are no longer needed
            foreach (int id in loaded_ids.Except(ids))
            {
                if (added_facts.ContainsKey(id))
                {
                    foreach (Fact fact in added_facts[id])
                        accessor.AddFact(fact);
                    added_facts.Remove(id);
                }
                if (deleted_facts.ContainsKey(id))
                {
                    foreach (Fact fact in deleted_facts[id])
                        accessor.DeleteFact(fact);
                    deleted_facts.Remove(id);
                }
                if (added_logs.ContainsKey(id))
                {
                    foreach (LogLine log in added_logs[id])
                        accessor.AddLog(log);
                    added_logs.Remove(id);
                }

                people.Remove(id);
                facts.Remove(id);
                logs.Remove(id);
            }
            // Add wanted unloaded records
            foreach (int id in ids.Except(loaded_ids))
            {
                Person person = new Person() { id = id };
                accessor.GetPerson(ref person);
                people[id] = person;
                facts[id] = accessor.GetFacts(id);
                logs[id] = accessor.GetLogs(id);
                added_facts[id] = new List<Fact>();
                deleted_facts[id] = new List<Fact>();
                added_logs[id] = new List<LogLine>();
            }
            loaded_ids = ids;
            loaded_ids = ids;
            EventLog.Log("Finished loading recognized people");
        }

        public bool GetPerson(int id, out Person person)
        {
            if (people.ContainsKey(id))
            {
                person = people[id];
                return true;
            }
            person = new Person() { id = id };
            return accessor.GetPerson(ref person);
        }

        public List<Fact> GetFacts(int id)
        {
            if (facts.ContainsKey(id))
                return facts[id];
            return accessor.GetFacts(id);
        }

        public List<LogLine> GetLogs(int id)
        {
            if (logs.ContainsKey(id))
                return logs[id];
            return accessor.GetLogs(id);
        }

        public bool GetImage(int id, out Image image)
        {
            return accessor.GetImage(id, out image);
        }

        public bool AddPair(int id1, int id2) => accessor.AddPair(id1, id2);

        public void AddFact(Fact fact)
        {
            if (loaded_ids.Contains(fact.person_id))
            {
                facts[fact.person_id].Add(fact);
                added_facts[fact.person_id].Add(fact);
            }
            else
                accessor.AddFact(fact);
        }

        public void DeleteFact(Fact fact)
        {
            if (loaded_ids.Contains(fact.person_id))
            {
                facts[fact.person_id].Remove(fact);
                deleted_facts[fact.person_id].Add(fact);
            }
            else
                accessor.DeleteFact(fact);
        }

        public void AddLog(LogLine log)
        {
            if (loaded_ids.Contains(log.person_id))
            {
                logs[log.person_id].Add(log);
                added_logs[log.person_id].Add(log);
            }
            else
                accessor.AddLog(log);
        }

        private Accessor accessor;
        private Dictionary<int, List<Fact>> facts;
        private Dictionary<int, List<Fact>> added_facts;
        private Dictionary<int, List<Fact>> deleted_facts;
        private Dictionary<int, List<LogLine>> logs;
        private Dictionary<int, List<LogLine>> added_logs;
        private Dictionary<int, Person> people;
        private List<int> loaded_ids;
    }

    public class Person
    {
        public int id { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public DateTime birthdate { get; set; }
    }

    public class LogLine
    {
        public LogLine(int person_id, in DateTime datetime, in string person_phrase, in string bot_phrase)
        {
            this.person_id = person_id;
            this.datetime = datetime;
            this.person_phrase = person_phrase;
            this.bot_phrase = bot_phrase;
        }
        public LogLine(in LogLine other)
        {
            person_id = other.person_id;
            datetime = other.datetime;
            person_phrase = other.person_phrase;
            bot_phrase = other.bot_phrase;
        }

        public int person_id { get; set; }
        public DateTime datetime { get; set; }
        public string person_phrase { get; set; }
        public string bot_phrase { get; set; }
}

    public class Fact : IEquatable<Fact>
    {
        public Fact(int person_id, in string fact)
        {
            this.person_id = person_id;
            this.fact = fact;
        }
        public Fact(in Fact other)
        {
            this.person_id = other.person_id;
            this.fact = other.fact;
        }

        public bool Equals(Fact other)
        {
            return (person_id == other.person_id && fact == other.fact);
        }

        public int person_id { get; set; }
        public string fact { get; set; }
    }    
}
