{
  "swagger": "2.0",
  "info": {
    "description": "Familliars recognition by photo",
    "version": "v1",
    "title": "Familliars Recognition",
    "contact": {
      "email": "ivanovdm@bmstu.ru"
    }
  },
  "host": "localhost",
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "Familliars",
      "description": "Getting familliars info"
    },
    {
      "name": "Facts",
      "description": "Managing facts about familliars"
    },
    {
      "name": "Logs",
      "description": "Adding dialogs recordings"
    }
  ],
  "paths": {
    "/Familliars": {
      "get": {
        "tags": [
          "Familliars"
        ],
        "summary": "Returns the list of IDs of people recognised on a photo given in request body",
        "operationId": "Get",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "image",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    },
    "/Familliars/{id}": {
      "get": {
        "tags": [
          "Familliars"
        ],
        "summary": "Returns a record of a person with the given ID",
        "operationId": "GetPerson",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      },
      "patch": {
        "tags": [
          "Familliars"
        ],
        "summary": "Replaces person's facts if they differ",
        "operationId": "Patch",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "facts",
            "in": "body",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Fact"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "400": {
            "description": "Bad Request"
          }
        }
      }
    },
    "/api/v1/Familliars/{id}/Facts": {
      "post": {
        "tags": [
          "Facts"
        ],
        "summary": "Adds a new fact to database",
        "operationId": "PostFact",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "fact",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      },
      "delete": {
        "tags": [
          "Facts"
        ],
        "summary": "Deletes a fact from database",
        "operationId": "Delete",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "fact",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    },
    "/api/v1/Familliars/{id}/Logs": {
      "post": {
        "tags": [
          "Logs"
        ],
        "summary": "Adds a new log line to database",
        "operationId": "PostLog",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "datetime",
            "in": "query",
            "required": true,
            "type": "string",
            "format": "date-time"
          },
          {
            "name": "phrases",
            "in": "body",
            "schema": {
              "type": "object",
              "$ref": "#/definitions/Phrases"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "api_key": {
      "type": "apiKey",
      "name": "api_key",
      "in": "header"
    }
  },
  "definitions": {
    "Phrases": {
      "type": "object",
      "properties": {
        "person_phrase": {
          "type": "string"
        },
        "bot_phrase": {
          "type": "string"
        }
      }
    },
    "Fact": {
      "type": "object",
      "properties": {
        "person_id": {
          "type": "integer",
          "format": "int32"
        },
        "fact": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "LogLine": {
      "type": "object",
      "properties": {
        "person_id": {
          "type": "integer",
          "format": "int32"
        },
        "datetime": {
          "type": "string",
          "format": "date-time"
        },
        "person_phrase": {
          "type": "string"
        },
        "bot_phrase": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "Person": {
      "type": "object",
      "properties": {
        "facts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Fact"
          }
        },
        "logs": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/LogLine"
          }
        },
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "name": {
          "type": "string"
        },
        "sex": {
          "type": "string"
        },
        "birthdate": {
          "type": "string",
          "format": "date-time"
        }
      },
      "additionalProperties": false
    }
  }
}