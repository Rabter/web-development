﻿using System;
using System.IO;

public static class EventLog
{
    public enum Type
    {
        Information = 0,
        Exception,
        Error,
        CriticalError
    }

    public static string logFile = "log.txt";

    public static void Log(string msg, Type type = 0)
    {
        using (StreamWriter writer = new StreamWriter(logFile, append: true))
        {
            writer.AutoFlush = true;            
            writer.Write(DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss:ff") + ' ');
            switch (type)
            {
                case Type.Information:
                    writer.Write("INFORMATION: ");
                    break;
                case Type.Exception:
                    writer.Write("EXCEPTION: ");
                    break;
                case Type.Error:
                    writer.Write("ERROR: ");
                    break;
                case Type.CriticalError:
                    writer.Write("CRITICAL ERROR: ");
                    break;
            }
            writer.WriteLine(msg);
        }
    }
}
