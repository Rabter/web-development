import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilliarsListPageComponent } from './familliars-list-page.component';

describe('FamilliarsListPageComponent', () => {
  let component: FamilliarsListPageComponent;
  let fixture: ComponentFixture<FamilliarsListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FamilliarsListPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilliarsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
