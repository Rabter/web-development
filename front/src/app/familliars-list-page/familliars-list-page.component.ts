import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Person } from '../entity/Person';

@Component({
  selector: 'app-familliars-list-page',
  templateUrl: './familliars-list-page.component.html',
  styleUrls: ['./familliars-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FamilliarsListPageComponent implements OnInit {

  constructor(private http: HttpClient) { }

  id = '';
  title = 'Get facts';
  factsListSize = 10;
  facts = Array<string>(this.factsListSize);
  ngOnInit(): void {
  }

  onGet(): void
  {
    this.http.get(`${environment.apiURL}/Familliars/${this.id}`).subscribe((response: Person) =>
    {
      this.facts = [];
      let count = response.facts.length;
      while (count--) { this.facts.push(response.facts[count].fact); }
    }, (error) => console.log(error));
  }
}
