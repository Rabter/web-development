import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { FamilliarsListPageComponent } from './familliars-list-page/familliars-list-page.component';

const appRoutes: Routes = [
  { path: '', component: FamilliarsListPageComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    FamilliarsListPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
