import { Fact } from './Fact';
import { Log } from './Log';

export class Person
{
  facts: Array<Fact>;
  logs: Array<Log>;
  photo: object;
  id: number;
  name: string;
  sex: string;
  birthdate: string;
}
