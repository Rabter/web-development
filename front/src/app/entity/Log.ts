export class Log
{
  personId: number;
  datetime: string;
  personPhrase: string;
  botPhrase: string;
}
