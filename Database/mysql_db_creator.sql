create database Familliars;

use Familliars;

drop table People;
drop table Facts;
drop table Logs;


create table People(
    id int not NULL primary key,
	name varchar(20),
	sex char(1),
	birthdate date,
	picture varchar(50) 
	);

create table Facts(
	person_id int not NULL,
	fact varchar(40),
	foreign key (person_id) references People(id)
	);

create table Logs(
	person_id int not NULL,
	log_time datetime,
	person_phrase text,
	bot_phrase text,
	foreign key (person_id) references People(id)
	);

create table CommonFamilliars(
    id1 int not null,
	id2 int not null
	);

truncate table People;
truncate table Facts;
truncate table Logs;
truncate table CommonFamilliars;

insert into People(id, name, sex, birthdate, picture) values(1, 'Ivanov Dmitry', 'm', '2000-03-07', '');
insert into People(id, name, sex, birthdate, picture) values(2, 'Sheldon Cooper', 'm', '1980-02-26', 'D:\\University\\DB_Project\\Database\\Sheldon.jpg');
insert into People(id, name, sex, birthdate, picture) values(3, 'Sarah Conor', 'f', '1965-01-01', 'D:\\University\\DB_Project\\Database\\Sarah.jpg');
insert into People(id, name, sex, birthdate, picture) values(4, 'Leonard Hofstadter', 'm', '1980-05-17', 'D:\\University\\DB_Project\\Database\\Leonard.jpg');
insert into People(id, name, sex, birthdate, picture) values(5, 'Bernadette', 'f', '1984-01-01', 'D:\\University\\DB_Project\\Database\\Bernadette.jpg');
insert into People(id, name, sex, birthdate, picture) values(6, 'Howard Wolowitz', 'm', '1981-09-30', 'D:\\University\\DB_Project\\Database\\Howard.jpg');
insert into People(id, name, sex, birthdate, picture) values(7, 'Amy Farrah Fowler', 'f', '1979-12-17', 'D:\\University\\DB_Project\\Database\\Amy.jpg');
insert into People(id, name, sex, birthdate, picture) values(8, 'Rajesh Koothrappali', 'm', '1981-10-06', 'D:\\University\\DB_Project\\Database\\Raj.png');
insert into People(id, name, sex, birthdate, picture) values(9, 'Penny', 'f', '1985-12-02', 'D:\\University\\DB_Project\\Database\\Penny.jpg');

insert into Facts(person_id, fact) values(1, "Studies at BMSTU");
insert into Facts(person_id, fact) values(3, "Chased by a robot");
insert into Facts(person_id, fact) values(2, "Nerd");
insert into Facts(person_id, fact) values(2, "PhD at Physics");
insert into Facts(person_id, fact) values(1, "Always lacks in time");
insert into Facts(person_id, fact) values(4, "Lacose intolerance");
insert into Facts(person_id, fact) values(9, "Married Leonard");
insert into Facts(person_id, fact) values(8, "Has a pet dog");
insert into Facts(person_id, fact) values(7, "In love with Sheldon");
insert into Facts(person_id, fact) values(4, "Married Penny");
insert into Facts(person_id, fact) values(5, "Married Howard");
insert into Facts(person_id, fact) values(6, "Married Bernadette");
insert into Facts(person_id, fact) values(6, "Went to space");


insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(1, "2020-05-09 21:23", "I wish I had more time.", "But you are on holidays!");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(1, "2020-05-09 21:24", "And in deadlines.", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(3, "2020-05-09 10:15:20", "", "Why are you panting?");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(3, "2020-05-09 10:15:23", "Because I was running.", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(3, "2020-05-09 10:15:25", "", "Why were you running?");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(3, "2020-05-09 10:15:27", "I am chased by a robot.", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(2, "2020-06-05 10:15:27", "I am surrounded by idiots", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(2, "2020-06-05 10:25:27", "I am surrounded by idiots", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(6, "2020-06-05 01:02:03", "I went to space", "");
insert into Logs(person_id, log_time, person_phrase, bot_phrase) values(2, "2020-06-06 10:15:27", "I am surrounded by idiots", "");

select * from People;
select * from Facts;
select * from Logs;
select * from CommonFamilliars;

select * from Logs where person_id = 6 and log_time = '07-06-2020';
delete from Logs where person_id = 2 and log_time = '07-06-2020';